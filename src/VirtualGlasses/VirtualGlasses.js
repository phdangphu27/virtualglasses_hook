import React, { useState } from "react";
import GlassesList from "./GlassesList/GlassesList";
import { dataGlasses } from "../dataGlasses/dataGlasses";
import ModelWithGlasses from "./ModelWithGlasses/ModelWithGlasses";
import bg from "../assets/background.jpg";

const VirtualGlasses = () => {
  const [glasses, setGlasses] = useState(dataGlasses[0]);

  const handleChangeGlasses = (obj) => {
    setGlasses(obj);
  };

  return (
    <div className='container'>
      <ModelWithGlasses glasses={glasses} />
      <GlassesList
        glassesData={dataGlasses}
        handleChangeGlasses={handleChangeGlasses}
      />
    </div>
  );
};

export default VirtualGlasses;
