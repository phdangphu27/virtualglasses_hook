import React from "react";

const ModelWithGlasses = (props) => {
  return (
    <div style={{ border: "1px solid black", width: "400px" }}>
      <img
        className='modelGlasses'
        alt='true'
        style={{
          height: "80px",
          width: "230px",
        }}
        src={props.glasses.url}
      />
      <img
        alt='true'
        src='./glassesImage/model.jpg'
        style={{ height: "400px", width: "400px" }}
      />
      <div className='info'>
        <p>{props.glasses.name}</p>
        <p>{props.glasses.desc}</p>
      </div>
    </div>
  );
};

export default ModelWithGlasses;
