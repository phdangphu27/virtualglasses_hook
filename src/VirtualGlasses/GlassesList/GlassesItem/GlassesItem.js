import React from "react";

const GlassesItem = (props) => {
  return (
    <div
      className='glasses p-3'
      onClick={() => {
        props.handleChangeGlasses(props.glasses);
      }}
    >
      <img
        className='h-100'
        style={{
          height: "80px",
          width: "230px",
        }}
        alt='true'
        src={props.glasses.url}
      />
    </div>
  );
};

export default GlassesItem;
