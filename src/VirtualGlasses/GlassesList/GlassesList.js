import React from "react";
import GlassesItem from "./GlassesItem/GlassesItem";

const GlassesList = (props) => {
  const renderGlasses = () => {
    return props.glassesData.map((glasses) => {
      return (
        <div className='row'>
          <GlassesItem
            handleChangeGlasses={props.handleChangeGlasses}
            glasses={glasses}
          />
        </div>
      );
    });
  };

  return (
    <div>
      <div className='list'>{renderGlasses()}</div>
    </div>
  );
};

export default GlassesList;
